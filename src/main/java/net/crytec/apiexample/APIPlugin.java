package net.crytec.apiexample;

import co.aikar.commands.BukkitCommandManager;
import net.crytec.apiexample.inventories.InventoryCommand;
import net.crytec.apiexample.scoreboards.ScoreboardCommands;
import net.crytec.apiexample.scoreboards.ScoreboardListener;
import net.crytec.inventoryapi.InventoryAPI;
import net.crytec.libs.commons.utils.CommonsAPI;
import net.crytec.libs.protocol.scoreboard.ScoreboardAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class APIPlugin extends JavaPlugin {

  private ScoreboardAPI scoreboardAPI;

  @Override
  public void onEnable() {
    // Initialize Scoreboard API and save it as a variable, we need it later
    scoreboardAPI = new ScoreboardAPI(this, false);
    // Initialize CommonsAPI
    new CommonsAPI(this);
    // Initialize InventoryAPI
    new InventoryAPI(this);
    // Initialize the command manager
    BukkitCommandManager commandManager = new BukkitCommandManager(this);
    commandManager.registerCommand(new ScoreboardCommands(this));
    Bukkit.getPluginManager().registerEvents(new ScoreboardListener(this), this);

    // Example Inventory Command
    commandManager.registerCommand(new InventoryCommand());

  }

  @Override
  public void onDisable() {

  }

  public ScoreboardAPI getBoardAPI() {
    return this.scoreboardAPI;
  }

}
