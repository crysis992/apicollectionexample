package net.crytec.apiexample.inventories;

import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.chatinput.ChatInput;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

public class ExampleInventory1 implements InventoryProvider {

  private String name = "none";

  @Override
  public void init(Player player, InventoryContent content) {
    content.fillBorders(ClickableItem.empty(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build()));

    content.set(SlotPos.of(1, 3), ClickableItem.of(new ItemBuilder(Material.COOKIE).name("Heilung").build(), click -> {
      player.setFoodLevel(20);
      player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
      player.sendMessage(ChatColor.GREEN + " Du wurdest geheilt!");
    }));

    content.set(SlotPos.of(1, 6), ClickableItem.of(new ItemBuilder(Material.NAME_TAG).name("Chat Input")
        .lore("Dein Input war: " + (name.equals("none") ? "Nichts" : name))
        .build(), click -> {
      new ChatInput(player, "Bitte gebe einen Wert ein:", true, input -> {
        name = input;
        reopen(player, content);
      });
    }));
  }
}
