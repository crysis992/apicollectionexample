package net.crytec.apiexample.inventories;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Subcommand;
import net.crytec.inventoryapi.SmartInventory;
import org.bukkit.entity.Player;

@CommandAlias("cinv")
public class InventoryCommand extends BaseCommand {

  @Subcommand("inv1")
  public void showTestInt1(Player issuer) {
    SmartInventory.builder().size(5).title("Example Inventory").provider(new ExampleInventory1()).build().open(issuer);
  }
}
