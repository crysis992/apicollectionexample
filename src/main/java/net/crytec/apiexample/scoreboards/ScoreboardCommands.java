package net.crytec.apiexample.scoreboards;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import com.google.common.collect.Sets;
import java.util.Set;
import net.crytec.apiexample.APIPlugin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

@CommandAlias("cboard")
public class ScoreboardCommands extends BaseCommand {

  private final APIPlugin plugin;

  private final Set<Player> activeBoards = Sets.newHashSet();

  public ScoreboardCommands(APIPlugin plugin) {
    this.plugin = plugin;
  }

  @Default
  public void activateBoard(Player issuer) {

    if (activeBoards.contains(issuer)) {
      plugin.getBoardAPI().getBoardManager().getBoard(issuer).deactivate();
      issuer.sendMessage(ChatColor.RED + "Board deaktiviert");
      return;
    }

    plugin.getBoardAPI().getBoardManager().setBoard(issuer, new ExampleBoard());
    plugin.getBoardAPI().getBoardManager().getBoard(issuer).activate();
    issuer.sendMessage(ChatColor.GREEN + "Board aktiviert");
  }
}
