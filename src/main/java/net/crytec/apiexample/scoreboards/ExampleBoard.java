package net.crytec.apiexample.scoreboards;

import java.util.List;
import net.crytec.libs.commons.utils.UtilLoc;
import net.crytec.libs.protocol.scoreboard.api.Entry;
import net.crytec.libs.protocol.scoreboard.api.EntryBuilder;
import net.crytec.libs.protocol.scoreboard.api.ScoreboardHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ExampleBoard implements ScoreboardHandler {

  @Override
  public String getTitle(Player player) {
    return player.getName();
  }

  @Override
  public List<Entry> getEntries(Player player) {

    return new EntryBuilder()
        .blank()
        .next(ChatColor.YELLOW + "Rang: Blub")
        .blank()
        .next(ChatColor.RED + "Time: " + System.currentTimeMillis())
        .blank()
        .next("Pos: " + UtilLoc.getLogString(player.getLocation()))
        .build();
  }
}
