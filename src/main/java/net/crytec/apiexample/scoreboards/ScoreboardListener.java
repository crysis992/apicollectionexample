package net.crytec.apiexample.scoreboards;

import net.crytec.apiexample.APIPlugin;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ScoreboardListener implements Listener {

  private final APIPlugin plugin;

  public ScoreboardListener(APIPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void setupPlayerOnJoin(PlayerJoinEvent event) {
    plugin.getBoardAPI().getBoardManager().addPlayer(event.getPlayer(), "100");

    // Über den Boardmanager kannst du das Prefix und Suffix eines Spielers setzen
    // Das hier gesetzt Prefix & Suffix wird 'über' dem Spieler und in der Tablist angezeigt
    plugin.getBoardAPI().getBoardManager().setPrefix(event.getPlayer(), ChatColor.RED + "Admin und so ");
    plugin.getBoardAPI().getBoardManager().setSuffix(event.getPlayer(), ChatColor.YELLOW + " Blub ");

  }

  @EventHandler
  public void unregisterPlayer(PlayerQuitEvent event) {
    plugin.getBoardAPI().getBoardManager().unregisterPlayer(event.getPlayer());
  }

}
